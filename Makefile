MCU          = atmega32u4
ARCH         = AVR8
BOARD        = USER
F_CPU        = 16000000
F_USB        = 16000000
OPTIMIZATION = 3
TARGET       = touchit
LUFA_PATH    = lufa/LUFA
CC_FLAGS     = -DUSE_LUFA_CONFIG_HEADER -Iconfig/ -Wall
#CC_FLAGS     += -Werror
#CC_FLAGS     += -DUART_DEBUG
LD_FLAGS     =
SRC          = \
	touchit.c \
	ftdi.c \
	led.c \
	$(LUFA_SRC_USB)

# Default target
.PHONY: deftarget
#deftarget: process
#deftarget: $(TARGET).hex size check
deftarget: $(TARGET).hex size dfu

# Assume just one device:
DEVICE = /dev/serial/by-id/usb-LUFA_FT232_*
DEBUG_DEVICE = /dev/serial/by-id/usb-FTDI_FT230X_USB_Half_UART_DC001VW3-if00-port0

# Misc helpers

# Build documentation
%.html: %.md
	pandoc -o $@ -f markdown -t html --standalone --smart $<

# Check for hex file differences (used during refactoring)
check:
	cmp $(TARGET).hex $(TARGET).good
.PHONY: check

# Open terminal on USB port
term:
	python terminal.py $(DEVICE)
.PHONY: term

# Open terminal on debug serial port
debugterm:
	python terminal.py $(DEBUG_DEVICE) 1000000
.PHONY: debugterm



# Autogenerate version from git
.PHONY: .FORCE
version.h: .FORCE
	@echo "#define GIT_VERSION \"$(shell git describe --long --dirty)\"" > version-new.h
	@cmp -s version-new.h version.h || cp -f version-new.h version.h
	@rm -f version-new.h
# version.h is a generated file, so list dependencies on it manually.
accelog.c: version.h
manager.c: version.h



# Include LUFA build script makefiles
include $(LUFA_PATH)/Build/lufa_core.mk
include $(LUFA_PATH)/Build/lufa_sources.mk
include $(LUFA_PATH)/Build/lufa_build.mk
include $(LUFA_PATH)/Build/lufa_cppcheck.mk
include $(LUFA_PATH)/Build/lufa_avrdude.mk
include $(LUFA_PATH)/Build/lufa_atprogram.mk

# Remove some crap from BASE_CC_FLAGS that the LUFA core put in there.
BASE_CC_FLAGS := $(filter-out -fno-inline-small-functions,$(BASE_CC_FLAGS))
#BASE_CC_FLAGS := $(filter-out -fno-strict-aliasing,$(BASE_CC_FLAGS))

dfu: $(TARGET).hex $(MAKEFILE_LIST)
	@echo $(MSG_DFU_CMD) Programming FLASH with dfu-programmer using \"$<\"
	dfu-programmer $(MCU) erase
	dfu-programmer $(MCU) flash $<
	dfu-programmer $(MCU) reset
	-dfu-programmer $(MCU) reset
