#include <avr/io.h>
#include <avr/interrupt.h>


#include <ctype.h>
#include "led.h"


#define a 0x80
#define b 0x40
#define c 0x20
#define d 0x10
#define e 0x08
#define f 0x04
#define g 0x02
#define h 0x01

const char char_gen[] = {
 a+b+c+d+e+f,   // Displays "0"
 b+c,           // Displays "1"
 a+b+d+e+g,     // Displays "2"
 a+b+c+d+g,     // Displays "3"
 b+c+f+g,       // Displays "4"
 a+c+d+f+g,     // Displays "5"
 a+c+d+e+f+g,   // Displays "6"
 a+b+c,         // Displays "7"
 a+b+c+d+e+f+g, // Displays "8"
 a+b+c+d+f+g,   // Displays "9"
 a+b+c+e+f+g,   // Displays "A"
 0x00,          // Displays  Blank
 a+d+e+f,       // Displays "C"
 a+b+f+g,       // Displays "degrees" o
 a+d+e+f+g,     // Displays "E"
 a+e+f+g        // Displays "F"   
 };


#undef a
#undef b
#undef c
#undef d
#undef e
#undef f
#undef g
#undef h

void led_init()
{
  DDRB= SER | SCK | RCK;
  PORTB &= ~(SER | SCK | RCK);
  //set up 8 bit timer for CTC mode PWM
  TCCR0A = 0x0;
  DDRD |= ( 1<<0 ) | (1 << 1);
  TCCR0A |= (1 << 1); //CTC mode
  TCCR0A |= (1 << 4); //toggle OC0B

  TCCR0B = 0x0;
  TCCR0B |= (1 << 0) | (1 << 1); //no clock prescaling (8 MHz)
  OCR0A = 0xFF; //set PWM period 8MHz/256 
  OCR0B = 0x05; //set PWM duty cycle to 50%
  TIMSK0 = 0x0;
  TIMSK0 |= (1 << 2 ) | (1 << 0); //interrupt on output compare B and OVF

  //set up 16 bit timer for CTC mode PWM
  TCCR1B = 0x0;
  TCCR1B |= (1 << 3); //CTC mode TOP=OCR1A
  TCCR1B |= (1 << 0); //no clock prescaler (8 MHz)
  OCR1A = 0xFF; //set PWM period to 8MHz/256
  OCR1B = 0x08; //set PWM duty cycle to 50%
  TIMSK1 = 0x0; 
  //TIMSK1 = (1 << 2) | (1 << 0);
  //  sei();
}

void led_load_data(uint32_t data)
{
  int i;
  
  for(i=0;i<32;i++){
    PORTB &= ~SCK;
    if (data & 1)
      PORTB &= ~SER;
    else 
      PORTB |= SER;
    data >>= 1;
    PORTB |= SCK;
  }
  //pulse the RCK line to latch registers and enable the output
  PORTB &= ~ RCK;
  PORTB |= RCK;
  PORTB &= ~ RCK;
}

uint32_t hex_data=0x0;

void led_write_number(int data){
  hex_data=0x0;
  int ones,tens,hundreds,thousands;
  ones=data%10;
  data-=ones;
  tens=(data%100)/10;
  data-=tens;
  hundreds=(data%1000)/100;
  data-=hundreds;
  thousands=data/1000;
  hex_data=
    (uint32_t)(char_gen[thousands])<<24 |
    (uint32_t)(char_gen[hundreds])<<16 |
    (uint32_t)(char_gen[tens])<<8 |
    (uint32_t)(char_gen[ones]);
  led_load_data(hex_data);
}

ISR(TIMER0_COMPB_vect){
  led_load_data(0x0);
  PORTD &= ~(1 << 1);
  TIFR0 |= (1 << 2);
}
ISR(TIMER0_OVF_vect){
  led_load_data(hex_data);//hex_data);

  PORTD |= (1<<1);//~PORTD;
  TIFR0 |= (1 << 1);
}


/*
void write_string(uint8_t *string)
{
  uint32_t data = 0;
  int i;
  for (i = 0; i < 4; i++) {
    data <<= 8;
    data |= char_gen[string[i]];
  }
  led_load_data(hex_data);
}
*/
/*char buffer[4];
sprintf(buffer, "%04d", data);
write_string(buffer);
*/


  
     
