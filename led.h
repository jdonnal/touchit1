#ifndef LEDS_H
#define LEDS_H

#define SER (1 << 6)
#define SCK (1 << 5)
#define RCK (1 << 4)


//from http://www.piclist.com/techref/microchip/language/C/7segment_C.htm

#define LED_0 a+b+c+d+e+f
#define LED_1 char_gen[1]
#define LED_2 char_gen[2]
#define LED_3 char_gen[3]
#define LED_4 char_gen[4]
#define LED_5 char_gen[5]
#define LED_6 char_gen[6]
#define LED_7 char_gen[7]
#define LED_8 char_gen[8]
#define LED_9 char_gen[9]
#define LED_A char_gen[10]
#define LED_BLANK char_gen[11]
#define LED_C char_gen[12]
#define LED_DEGREES char_gen[13]
#define LED_E char_gen[14]
#define LED_F char_gen[15]



void led_load_data(uint32_t data);
void led_init(void);

#endif
