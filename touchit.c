#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "led.h"
#include "ftdi.h"

#include <LUFA/Drivers/USB/USB.h>


static void setup(void)
{
  MCUSR=0; //disable watchdog timer
  wdt_disable();
  clock_prescale_set(clock_div_1);
  led_init();
  USB_Init();
  ftdi_init(FTDI_STDIO | FTDI_BLOCKING);
  
  DDRC |= (1 << 6);
  DDRC |= (1 << 7);
  sei();
}

int main(void)
{
  int data=123;
  setup();
  PORTC = 0xFF;

  while(1){
	  printf("Initialized.\n");
	  }


  PORTC &= ~(1 << 6);
  PORTC &= ~(1 << 7);

  led_write_number(data);
  for(;;){
    scanf("%d",&data);
    led_write_number(data);
    //data++;
    //if(data>9999)
    //  data=0;
  }
  return 0;
}
  
